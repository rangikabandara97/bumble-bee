package com.user.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.DAO.UserDAOInpl;
import com.DB.DBConnect;
import com.entity.User;


@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
	
		try {
			
			String name=req.getParameter("fname");
			String email=req.getParameter("email");
			String phno=req.getParameter("phno");
			String password=req.getParameter("password");
			String check=req.getParameter("check");
			
			
			
			User us=new User();
			us.setName(name);
			us.setEmail(email);
			us.setPhno(phno);
			us.setPassword(password);
			
			HttpSession session=req.getSession();
			
			if (check!=null) {
				
				UserDAOInpl dao=new UserDAOInpl(DBConnect.getConn());
				boolean f=dao.userRegister(us);
				if(f) 
				{
					//System.out.println("User Register Successfuly");
					session.setAttribute("succMsg", "Registration Successfully");
					resp.sendRedirect("Register.jsp");
				}
				else 
				{
					//System.out.println("something wrong");
					session.setAttribute("failedMsg", "something wrong");
					resp.sendRedirect("Register.jsp");
				}
			}
			else {
					//System.out.println("Please agree the Terms and Condition");
					session.setAttribute("failedMsg", "Please agree the Terms and Condition");
					resp.sendRedirect("Register.jsp");
				}
				
				
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
	
		
	}

	

}
}
