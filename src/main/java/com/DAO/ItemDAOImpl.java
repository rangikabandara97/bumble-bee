package com.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.entity.itemDtls;
import com.mysql.cj.protocol.Resultset;

public class ItemDAOImpl implements ItemDAO {
	
	private Connection conn;
	
	public ItemDAOImpl(Connection conn) {
		super();
		this.conn = conn;
	}


	public boolean addItem(itemDtls b) {
			boolean f=false;
		try {
			
			String sql = "insert into item_dtls(itemName, brand, price, itemCategory, status, photo,email) values(?,?,?,?,?,?,?)";
					PreparedStatement ps = conn.prepareStatement(sql) ;
					ps.setString(1, b.getItemName());
					ps.setString(2, b.getBrand());
					ps.setString(3, b.getPrice());
					ps.setString(4, b.getItemCategory());
					ps.setString(5, b.getStatus());
					ps.setString(6, b.getPhotoName());
					ps.setString(7, b.getEmail());

					int i = ps.executeUpdate();

					if (i == 1) {
					f = true;
					}	
		
			

			
			
			
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return f;
	}


	public List<itemDtls> getAllItems() {
		
		List<itemDtls> list = new ArrayList<itemDtls>();
		itemDtls b = null;
		
		
		try {
			
		   
			String sql = "select * from item_dtls";
			PreparedStatement ps = conn.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				b = new itemDtls();
				b.setItemId(rs.getInt(1));
				b.setItemName(rs.getString(2));
				b.setBrand(rs.getString(3));
				b.setPrice(rs.getString(4));
				b.setItemCategory(rs.getString(5));
				b.setStatus(rs.getString(6));
				b.setPhotoName(rs.getString(7));
				b.setEmail(rs.getString(8));
				list.add(b);
			}
			
			
			
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return list;
	}


	public List<itemDtls> getNewItems() {
		
		List<itemDtls> list = new ArrayList<itemDtls>();
		itemDtls b=null;
		try {
		String sql="select * from item_dtls where itemCategory=? and status=?";
		PreparedStatement ps=conn.prepareStatement(sql);
		ps. setString(1,"New");
		ps.setString(2,"Active");
		ResultSet rs=ps.executeQuery();
		int i=1;
		while(rs.next() && i<=4)
		{
			b.setItemId(rs.getInt(1));
		b.setItemName(rs.getString(2));
		b.setBrand(rs.getString(3));
		b.setPrice(rs.getString(4));
		b.setItemCategory(rs.getString(5));
		b.setStatus(rs.getString(6));
		b.setPhotoName(rs.getString(7));
		b.setEmail(rs.getString(8));
		list.add(b);
		i++;

		{
		b=new itemDtls();
		}
		}
		
		} catch (Exception e) {
		e.printStackTrace();
		}
		
		return null;
	



		}


	public boolean addItems(itemDtls b) {
		// TODO Auto-generated method stub
		return false;
	}


	public List<itemDtls> getAllitems() {
		// TODO Auto-generated method stub
		return null;
	}
	

}


