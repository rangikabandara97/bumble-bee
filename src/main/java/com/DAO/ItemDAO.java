package com.DAO;

import java.util.List;

import com.entity.itemDtls;

public interface ItemDAO {

		public boolean addItems(itemDtls b);
		
		public List<itemDtls> getAllitems(); 
		
		public List<itemDtls> getNewItems();
		
	}
