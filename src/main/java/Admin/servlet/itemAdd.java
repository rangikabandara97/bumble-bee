package Admin.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.DAO.ItemDAOImpl;
import com.DB.DBConnect;
import com.entity.itemDtls;

@WebServlet("/add_items")
@MultipartConfig
public class itemAdd extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		try {
			
			
						String itemName = req.getParameter("bname");
						String brand = req.getParameter("brand") ;
						String price = req.getParameter("price");
						String categories = req.getParameter("categories");
						String status = req.getParameter("status");
						Part part = req.getPart("bimg");
						String fileName = part.getSubmittedFileName();

						itemDtls b = new itemDtls(itemName, brand, price, categories, status, fileName, "admin");
						
						
						ItemDAOImpl dao=new ItemDAOImpl(DBConnect .getConn()) ;
						boolean f=dao.addItem(b);

						HttpSession session=req.getSession();

						if (f)

						{

						session.setAttribute("succMsg", "Item Add Sucessfully");
						resp. sendRedirect("Admin/add_items.jsp");

						}else {
						session.setAttribute("failedMsg","Something wrong on Serverf");
						resp. sendRedirect("Admin/add_items.jsp");
						
						}
						 
						
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

}

