<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BumbleBee- Item View</title>
<%@include file="all_component/allCSS.jsp"%>
</head>
<body>
	<%@include file="all_component/navbar.jsp"%>

	<div class="container p-3">
		<div class="row">

			<div class="col-md-6 text-center p-5 border bg-white">
				<img src="img/tshirt.jpg" style="height: 150px; width: 150px"><br>
				<h4 class="mt-3">
					Item Name: <span class="text-success">Waffle Thermal Shirt Tee</span>
				</h4>
				<h4>
					Brand Name:<span class="text-success">Levice</span>
				</h4>
				<h4>
					Category: <span class="text-success">New</span>
				</h4>

			</div>



			<div class="col-md-6 text-center p-5 border bg-white">
				<h2>Smart Watch</h2>
				<p>Color: white,black,grey,wine red,army green,light blue,dark <br>
				blueMaterial:cotton<br>
				Pattern:solid color<br>
				Garment:Hand washing or machine washing,line dry.</p>

				<div class="row">
					<div class="col-md4 text-danger text-center p-2">
						<i class="fas fa-money-bill-wave fa-2x"></i>
						<p>Cash On Delivery</p>
					</div>
					<div class="col-md4 text-danger text-center p-2">
						<i class="fas fa-money-bill-wave fa-2x"></i>
						<p>Return Item</p>
					</div>
					<div class="col-md4 text-danger text-center p-2">
						<i class="fas fa-money-bill-wave fa-2x"></i>
						<p>Free Shipping</p>
					</div>

				</div>
				<div class=" text-center p-3">
					<a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i>
						Add Cart</a> <a href="" class="btn btn-danger"><i
						class="fas fa-rupee-sign"></i>$35</a>

</div>


					
					
				</div>
			</div>
		</div>


<%@include file="all_component/footer.jsp" %>

</body>
</html>