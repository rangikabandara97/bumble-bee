<%@page import="com.DB.DBConnect"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BumbleBee</title>
<%@include file="all_component/allCSS.jsp"%>
<style type="text/css">
.back-img {
	background: url("img/eCommerce.jpg");
	height: 60vh;
	width: 100%;
	background-repeat: no repeat;
	background-size: cover;
}

.crd-ho:hover {
	background-color: #f5f5f5;
}
</style>
</head>

<body style="background-color: #ededed">
	<%@include file="all_component/navbar.jsp"%>

	
	
	<div class="container-fluid back-img">
		
	</div>
	
	

<!-- start resent items -->


	<div class="container ">
		<h3 class="text-center">Recent Items</h3>
		<div class="row">
			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/wattch.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Smart Watch</p>
						<p>Leather</p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-2"><i class="fa-solid fa-cart-shopping"></i> Add Cart</a> <a
								href="item1.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$80</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/tshirt.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Waffle Thermal Shirt Tee</p>
						<p>Levice</p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-2"><i class="fa-solid fa-cart-shopping"></i>Add Cart</a> <a
								href="item2.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$35</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/phone.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Galaxy Note10+</p>
						<p>Samsung</p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-1"><i class="fa-solid fa-cart-shopping"></i>Add Cart</a> <a
								href="item3.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$525</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/hdry.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Hair dryer</p>
						<p>Remington ionic</p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-2"><i class="fa-solid fa-cart-shopping"></i>Add Cart</a> <a
								href="item4.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$70</a>
						</div>
					</div>
				</div>
			</div>



			<div class="text-center mt-1 p-1">
				<a href="" class="btn btn-danger btn-sm text-white">View All</a>
			</div>


		</div>
	</div>

	<!-- end of resent items -->
	
	<hr>
	<!-- start new items -->
	
	

	<div class="container ">
		<h3 class="text-center">New Items</h3>
		<div class="row">
			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/lap.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Laptop Intel Core i7</p>
						<p>Lenovo IdeaPad</p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-1"><i class="fa-solid fa-cart-shopping"></i>Add Cart</a> <a
								href="nitem1.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$800</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/glass.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Men's SUNGLASSES</p>
						<p>SQUARE RETRO GOLD </p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-2"><i class="fa-solid fa-cart-shopping"></i>Add Cart</a> <a
								href="nitem2.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$22</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/cream.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Particle Mens Face Cream</p>
						<p>Moisturizer A+++</p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-1"><i class="fa-solid fa-cart-shopping"></i>Add Cart</a> <a
								href="nitem3.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$650</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/sub.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Shallow Truck Subwoofer</p>
						<p>Pioneer TS-A2000LB</p>
						<p>Categories:new</p>
						<div class="row">
							<a href="" class="btn btn-danger btn-sm ml-1"><i class="fa-solid fa-cart-shopping"></i>Add Cart</a> <a
								href="nitem4.jsp" class="btn btn-success btn-sm ml-1">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">299</a>
						</div>
					</div>
				</div>
			</div>



			<div class="text-center mt-1 p-1">
				<a href="" class="btn btn-danger btn-sm text-white">View All</a>
			</div>


		</div>
	</div>
	
	<!-- end of new items -->
	<hr>
	
	<!-- start Old items -->
	
	
	<div class="container ">
		<h3 class="text-center">Old Items</h3>
		<div class="row">
			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/bsgg.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Smart bsg</p>
						<p>Ukone</p>
						<p>Categories:Old</p>
						<div class="row">
							 <a
								href="" class="btn btn-success btn-sm ml-5">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$50</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/makeup.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Makeup Kit</p>
						<p>Cosme</p>
						<p>Categories:Old</p>
						<div class="row">
							 <a
								href="" class="btn btn-success btn-sm ml-5">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$45</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/bat.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Table Bats</p>
						<p>Wodtax</p>
						<p>Categories:Old</p>
						<div class="row">
							 <a
								href="" class="btn btn-success btn-sm ml-5">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$65</a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="card crd-ho">
					<div class="card-body text-center">
						<img alt="" src="img/presher.jpg"
							style="width: 150px; height: 200px" class="img-thumblin">
						<p>Blood Presher Machine</p>
						<p>Nauna</p>
						<p>Categories:Old</p>
						<div class="row">
							 <a
								href="" class="btn btn-success btn-sm ml-5">View Details</a> <a
								href="" class="btn btn-danger btn-sm ml-1">$90</a>
						</div>
					</div>
				</div>
			</div>



			<div class="text-center mt-1 p-1">
				<a href="" class="btn btn-danger btn-sm text-white">View All</a>
			</div>


		</div>
	</div>
	
	<!-- end of Old items -->
	
<%@include file="all_component/footer.jsp" %>

</body>
</html>