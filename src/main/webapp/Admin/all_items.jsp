<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ page import="com.DB.DBConnect"%>
<%@ page import="com.DAO.ItemDAOImpl"%>
<%@ page import="com.DAO.ItemDAO"%>
<%@ page import="java.util.List" %>
<%@ page import="com.entity.itemDtls" %>



<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin- All Items</title>
<%@include file="allCSS.jsp"%>
</head>
<body>
	<%@include file="navbar.jsp"%>



	<h3 class="text-center">Hello Admin</h3>

	<table class="table table-striped">
		<thead class="bg-primary">

			<tr>
				<th scope="col">ID</th>
				<th scope="col">Image</th>
				<th scope="col">Item Name</th>
				<th scope="col">Brand</th>
				<th scope="col">Price</th>
				<th scope="col">Categories</th>
				<th scope="col">States</th>
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>

			<%
			ItemDAO dao = new ItemDAOImpl(DBConnect.getConn());
			List<itemDtls> list = dao.getAllitems();
			for (itemDtls b : list) {
			%>

			<tr>
				<td><%=b.getItemId()%></td>
				<td><img src="../item/<%=b.getPhotoName()%>"
					style="width: 50px; height: 50px;"></td>
				<td><%=b.getItemName()%></td>
				<td><%=b.getBrand()%></td>
				<td><%=b.getPrice()%></td>
				<td><%=b.getItemCategory()%></td>
				<td><%=b.getStatus()%></td>
				<td><a href="#" class="btn btn-sm btn-primary">Edit</a> <a
					href="#" class="btn btn-sm btn-danger">Delete</a></td>
			</tr>

			<%
			}
			%>






		</tbody>
	</table>

	<div style="margin-top: 285px;">
		<%@include file="footer.jsp"%></div>

</body>
</html>