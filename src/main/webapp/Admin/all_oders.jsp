<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin- All Oders</title>
<%@include file="allCSS.jsp"%>
</head>
<body>
	<%@include file="navbar.jsp"%>



	<h3 class="text-center">Admin</h3>

	<table class="table table-striped">
		<thead class="bg-primary">

			<tr>
				<th scope="col">Orders Id</th>
				<th scope="col">Name</th>
				<th scope="col">Email</th>
				<th scope="col">Address</th>
				<th scope="col">Phone No</th>
				<th scope="col">Item Name</th>
				<th scope="col">Brand</th>
				<th scope="col">Price</th>
				<th scope="col">Payment Type</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th scope="row">1</th>
				<td>stephon</td>
				<td>Step@gmai.com</td>
				<td>mel Street,London</td>
				<td>21365416</td>
				<td>Blood Presher Machine</td>
				<td>Nauna</td>
				<td>$90</td>
				<td>online Payment</td>

			</tr>
			
			<tr>
				<th scope="row">2</th>
				<td>imran</td>
				<td>imm@gmai.com</td>
				<td>karachi,mombai</td>
				<td>21986514</td>
				<td>Men's SUNGLASSES</td>
				<td>SQUARE RETRO GOLD</td>
				<td>$22</td>
				<td>online Payment</td>

			</tr>
			
		</tbody>
	</table>

<div style="margin-top: 285px;">
<%@include file="footer.jsp" %></div>

</body>
</html>