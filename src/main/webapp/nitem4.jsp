<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BumbleBee- Item View</title>
<%@include file="all_component/allCSS.jsp"%>
</head>
<body>
	<%@include file="all_component/navbar.jsp"%>

	<div class="container p-3">
		<div class="row">

			<div class="col-md-6 text-center p-5 border bg-white">
				<img src="img/sub.jpg" style="height: 150px; width: 150px"><br>
				<h4 class="mt-3">
					Item Name: <span class="text-success">Shallow Truck Subwoofer</span>
				</h4>
				<h4>
					Brand Name:<span class="text-success">Pioneer TS-A2000LB</span>
				</h4>
				<h4>
					Category: <span class="text-success">New</span>
				</h4>

			</div>



			<div class="col-md-6 text-center p-5 border bg-white">
				<h2>Shallow Truck Subwoofer</h2>
				<p>sealed enclosure with one 8" TS-A2000 subwoofer.
5/8" medium density fiberboard construction.
covered with black carpet.
steel mesh grille.
glass fiber/mica reinforced polypropylene cone with rubber surround.
impedance : 2 ohms final load.
power handling: 250 watts RMS (700 watts peak power).
</p>

				<div class="row">
					<div class="col-md4 text-danger text-center p-2">
						<i class="fas fa-money-bill-wave fa-2x"></i>
						<p>Cash On Delivery</p>
					</div>
					<div class="col-md4 text-danger text-center p-2">
						<i class="fas fa-money-bill-wave fa-2x"></i>
						<p>Return Item</p>
					</div>
					<div class="col-md4 text-danger text-center p-2">
						<i class="fas fa-money-bill-wave fa-2x"></i>
						<p>Free Shipping</p>
					</div>

				</div>
				<div class=" text-center p-3">
					<a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i>
						Add Cart</a> <a href="" class="btn btn-danger"><i
						class="fas fa-rupee-sign"></i>$650</a>

</div>


					
					
				</div>
			</div>
		</div>


<%@include file="all_component/footer.jsp" %>

</body>
</html>